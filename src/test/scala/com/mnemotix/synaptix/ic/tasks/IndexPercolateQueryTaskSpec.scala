/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.synaptix.ic.tasks

import com.mnemotix.amqp.api.{AmqpClientConfiguration, AmqpMessage}
import com.mnemotix.synaptix.index.elasticsearch.models.PercolateQuery
import com.mnemotix.synaptix.{IndexHelper, SynaptixTestSpec}
import play.api.libs.json.Json

import scala.concurrent.duration._

class IndexPercolateQueryTaskSpec extends SynaptixTestSpec{

  override implicit val patienceConfig = PatienceConfig(10.seconds)
  val task = new IndexPercolateQueryTask("index.percolate", AmqpClientConfiguration.exchangeName)

  IndexHelper.bootstrapIndex()

  "IndexPercolateQueryTask" should {
    "execute a percolate query" in {
      val percoq = PercolateQuery(Seq(IndexHelper.indexName), "percoLabel", "John Doe is friend with Alice and Bob.", Some(0), Some(20))
      println(Json.prettyPrint(Json.toJson(percoq)))
      val message = AmqpMessage(Map.empty, Json.toJson(percoq))
      val resultMessage = task.onMessage(message.toReadResult()).futureValue
      val msg = Json.parse(resultMessage.bytes.utf8String)
      println(Json.prettyPrint(msg))
      val parsing = msg.validate[AmqpMessage]
      parsing.isSuccess shouldBe true
      println(parsing.get.body)
    }
  }
}