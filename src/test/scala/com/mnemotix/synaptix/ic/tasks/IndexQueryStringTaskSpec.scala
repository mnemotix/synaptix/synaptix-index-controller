/**
  * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.synaptix.ic.tasks

import com.mnemotix.amqp.api._
import com.mnemotix.synaptix.{IndexHelper, SynaptixTestSpec}
import play.api.libs.json._

import scala.concurrent.duration._

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 2019-05-15
  */

class IndexQueryStringTaskSpec extends SynaptixTestSpec {

  override implicit val patienceConfig = PatienceConfig(10.seconds)

  val task = new IndexQueryStringTask("index.query", AmqpClientConfiguration.exchangeName)

  IndexHelper.bootstrapIndex()

  "IndexQueryStringTask" should {
    "execute a search query" in {
      val qryStr = "email:\"john.doe@fake.com\""
      val message = AmqpMessage(Map.empty, JsString(qryStr))
      val resultMessage = task.onMessage(message.toReadResult()).futureValue
      val msg = Json.parse(resultMessage.bytes.utf8String).as[AmqpMessage]
      (msg.body \ "hits" \ "total").as[Int] shouldEqual 1
      val jsperson = (msg.body \ "hits" \ "hits" \\ "_source")(0).as[JsValue]
//      println(Json.prettyPrint(jsperson))
      (jsperson \ "fullName") .as[String] shouldEqual "John Doe"
    }
  }
}
