/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mnemotix.synaptix.ic.tasks

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.alpakka.amqp.{ReadResult, WriteMessage}
import com.mnemotix.amqp.api.AmqpMessage
import com.mnemotix.amqp.api.rpc.AmqpRpcTask
import com.mnemotix.exceptions.MessageParsingException
import com.mnemotix.synaptix.index.IndexClient
import com.mnemotix.synaptix.index.elasticsearch.models.ESIndexable
import play.api.libs.json.{JsString, Json}

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.{Failure, Success, Try}

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 2019-03-20
 */

class IndexDeleteDocTask(override val topic: String, override val exchangeName: String)(implicit override val system: ActorSystem, override val materializer: ActorMaterializer, override val ec: ExecutionContext) extends AmqpRpcTask {

  override def onMessage(msg: ReadResult, params: String*)(implicit ec: ExecutionContext): Future[WriteMessage] = Future {
    Json.parse(msg.bytes.utf8String).validate[AmqpMessage].isSuccess match {
      case true => {
        val content = Json.parse(msg.bytes.utf8String).as[AmqpMessage]
        val indexName: String = content.headers.get("indices").map(_.as[Seq[String]].head).getOrElse {
          logger.error(s"""Index name not found.\nAt least one index name should be given into the "indices" header.""")
          throw new IllegalArgumentException("Index name not found")
        }
        val indexable = content.body.as[ESIndexable]
        val future = IndexClient.deleteById(indexName, indexable.id).transform {
          case Success(resp) => Try {
            getResponseMessage(JsString(resp.body.getOrElse("")), "JSON", "OK", msg.properties)
          }
          case Failure(err) => Try {
            getErrorMessage(err, msg.properties)
          }
        }
        Await.result(future, 15.seconds)
      }
      case false => {
        logger.error(s"Unable to parse the message: ${msg.bytes.utf8String}")
        getErrorMessage(new MessageParsingException(s"Unable to parse the message: ${msg.bytes.utf8String}", null), msg.properties)
      }
    }
  }

}
