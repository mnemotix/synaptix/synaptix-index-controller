/**
  * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.synaptix.ic

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import com.mnemotix.amqp.api.AmqpClientConfiguration
import com.mnemotix.amqp.api.rpc.AmqpRpcController
import com.mnemotix.synaptix.index.IndexClient
import com.mnemotix.synaptix.ic.tasks._
import com.typesafe.scalalogging.LazyLogging

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext}

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 2019-03-20
  */

object IndexController extends App with LazyLogging {

  implicit val system = ActorSystem("IndexControllerSystem")
  implicit val materializer = ActorMaterializer()
  implicit val ec: ExecutionContext = system.dispatcher

  logger.info("Index controller starting...")

  IndexClient.init()

  val controller = new AmqpRpcController("Index Controller")
  controller.registerTask("admin.create.index", new AdminIndexCreateTask("admin.create.index", AmqpClientConfiguration.exchangeName))
  controller.registerTask("admin.drop.index", new AdminIndexDropTask("admin.drop.indexdrop", AmqpClientConfiguration.exchangeName))
  controller.registerTask("index.bulkInsert.docs", new IndexBulkInsertTask("index.bulkInsert.docs", AmqpClientConfiguration.exchangeName))
  controller.registerTask("index.delete.doc", new IndexDeleteDocTask("index.delete.doc", AmqpClientConfiguration.exchangeName))
  controller.registerTask("index.insert.doc", new IndexInsertDocTask("index.insert.doc", AmqpClientConfiguration.exchangeName))
  controller.registerTask("index.query", new IndexQueryStringTask("index.query", AmqpClientConfiguration.exchangeName))
  controller.registerTask("index.search", new IndexRawQueryTask("index.search", AmqpClientConfiguration.exchangeName))
  controller.registerTask("index.update.doc", new IndexUpdateDocTask("index.update.doc", AmqpClientConfiguration.exchangeName))
  controller.registerTask("index.percolate", new IndexPercolateQueryTask("index.percolate", AmqpClientConfiguration.exchangeName))
  val starting = controller.start(true)

  logger.info("Index controller started successfully.")

  sys addShutdownHook {
    IndexClient.shutdown()
    Await.result(controller.shutdown, Duration.Inf)
  }
}
