import Dependencies._
import sbt.url

val meta = """META.INF(.)*""".r

lazy val root = (project in file("."))
  .settings(
    name := "synaptix-index-controller",
    description := "Synaptix Index Controller",
    homepage := Some(url("https://gitlab.com/mnemotix/synaptix/synaptix-index-controller")),
    scmInfo := Some(
      ScmInfo(
        url("https://gitlab.com/mnemotix/synaptix/synaptix-index-controller.git"),
        "git@gitlab.com:mnemotix/synaptix/synaptix-index-controller.git"
      )
    ),
    mainClass in(Compile, run) := Some("com.mnemotix.synaptix.ic.IndexController"),
    mainClass in assembly := Some("com.mnemotix.synaptix.ic.IndexController"),
    assemblyJarName in assembly := "synaptix-index-controller.jar",
    libraryDependencies ++= Seq(
      scalaTest % Test,
      logbackClassic,
      akkaStreamTestkit % Test,
      elastic4sTestkit % Test,
      indexingToolkit,
      amqpLib
    ),
    test in assembly := {},
    assemblyMergeStrategy in assembly := {
      case PathList("javax", "servlet", xs@_*) => MergeStrategy.first
      case PathList(ps@_*) if ps.last endsWith ".html" => MergeStrategy.first
      //        case n if n.startsWith("reference.conf") => MergeStrategy.concat
      case n if n.endsWith(".conf") => MergeStrategy.concat
      case n if n.endsWith(".properties") => MergeStrategy.concat
      case PathList("META-INF", "services", "org.apache.jena.system.JenaSubsystemLifecycle") => MergeStrategy.concat
      case PathList("META-INF", "services", "org.apache.spark.sql.sources.DataSourceRegister") => MergeStrategy.concat
      case PathList("META-INF", "services", xs@_*) => MergeStrategy.concat
      case PathList("META-INF", xs@_*) => MergeStrategy.discard
      case meta(_) => MergeStrategy.discard
      case x => MergeStrategy.first
    },
    imageNames in docker := Seq(
      ImageName(
        namespace = Some("registry.gitlab.com/mnemotix/synaptix"),
        repository = artifact.value.name,
        tag = Some(version.value)
      )
    ),
    buildOptions in docker := BuildOptions(cache = false),
    dockerfile in docker := {
      // The assembly task generates a fat JAR file
      val artifact: File = assembly.value
      val artifactTargetPath = s"/app/${artifact.name}"

      new Dockerfile {
        from("adoptopenjdk/openjdk11:alpine-slim")
        add(artifact, artifactTargetPath)
        entryPoint("java", "-jar", artifactTargetPath)
      }
    }
  )
  .enablePlugins(DockerPlugin)